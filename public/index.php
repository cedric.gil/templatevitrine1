<?php

// Démarrage de la session PHP
session_start();

// Affichage des erreurs PHP
ini_set('display_errors', 'on');

// Définition des variables de navigations
if(isset($_GET['p'])){
    $page = $_GET['p'];
    if(isset($_GET['sp'])){
        $spage = $_GET['sp'];
    }
    else{
        $spage = null;
    }
}
else{
    $page = 'home';
    $spage = null;
}

// Appel des Autoloaders
require '../vendor/autoload.php';
require '../app/Autoloader.php';
MyAutoloader::register();

// Appel du fichier PHP propre à la page actuelle, et définition des paramètres envoyés à Twig
$phppath = Engine::TwigPhpPath($page, $spage);
$paramsfromphpfile = [];
if($phppath != false){
    require $phppath;
    if(isset($params)){
        $paramsfromphpfile = $params;
    }
}

// Affichage du rendu Twig
echo Engine::TwigRender($page, $spage, $paramsfromphpfile)

?>